# Jmeter GitLab Library


Gitlab include library for running jmeter tests from GitLab CI Pipelines

| Author | Library Version |  Comment |
| ----- | --------------- | ------ |
| [@kristofre](https://gitlab.com/kristofre)| v1.0 | Initial Release |

## Stages

> Note: there is a full sample pipeline in the samples directory
### Keptn Init
Keptn needs to be initialized (project, stage, service, SLOs, SLIs, monitoring, ...) to inform Keptn about what is going to be evaluated.

For that, you can include the `keptn_init.yaml` file from this repo into you `.gitlab-ci.yaml` file:
```yaml
include:
  - remote: 'https://gitlab.com/dynatrace-ace/jmeter-gitlab-library/-/raw/master/jmeter_run_test.yaml'
```
> Note: make sure to specify the correct release, as master is a working branch

Once included, you can add the stage to the `.gitlab-ci.yaml` file:

```yaml
run-tests:
  extends: .run-jmeter
  stage: test   
  environment:
    name: test
  variables: 
    SERVER_URL: myapp.com
    SERVER_PORT: 80
    VUCOUNT: 2
    LOOPCOUNT: 1
    THINKTIME: 100
    TEST_FILE: jmeter/simple_load.jmx
```

The following variables can be set, either on the job, pipeline or global level:
| Variable  | Description | Required |
| --------- | ----------- | -------- |
| SERVER_URL | The endpoint you want to test. e.g.: dynatrace.com | **yes** | 
| SERVER_PORT | The port you want to test. e.g.: 443| **yes** | 
| VUCOUNT | Number of VU. e.g.: 100 | **yes** | 
| LOOPCOUNT | The number of loops. e.g.: 10 | **yes** | 
| THINKTIME | The think time. e.g. 100 | **yes** | 
| TEST_FILE | JMX Test file you want to load. e.g.: jmeter/simple_load.jmx | **yes** |  
   